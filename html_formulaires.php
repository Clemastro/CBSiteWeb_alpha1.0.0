<?php
    
    //-----Formulaires-----
    //<link href="style_formulaires.css" rel="stylesheet"/>
    //Création du Login

    function generate_login_formulaire(){
        $html='            
            <section class="center-box">
                <form id="login">
                    <h1 id="login_titre">LOGIN</h1>
                    <h3>Email</h3>
                    <input id="login_email" type="text" size="40">
                    <h3>Mot de passe</h3>
                    <input id="login_mdp" type="password" size="40">
                    <button id="login_button">Valider</button>
                    <div id="login_subbutton">
                        <button id="login_subbutton_nologin">Entrer sans se connecter</button>
                        <button id="login_subbutton_register">Créer un compte</button>
                    </div>
                </form>
            </section>
        ';
        return $html;
    }


?>