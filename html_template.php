<?php
    require("html_formulaires.php");
    //----------Créations de templates----------

    //-----Page-----

    //Création de la page HTML
    function generate_html_page($title, $stylesheet, $script){
        $html='
        <html>'
        .generate_header($title,$stylesheet).
        ''
        .generate_body($script).      
        '</html>';

        return $html;
    }

    //Création de l'entête
    function generate_header($title, $stylesheet){
        $head='
            <head>
            <meta charset="UTF-8"/>
            <title>'.$title.'</title>
            <link href="'.$stylesheet.'" rel="stylesheet" type="text/css"/>
            <link rel="stylesheet" href="normalize.css">
            </head>
        ';
        return $head;
    }

    //Création du body
    function generate_body($script){
        $body='
            <body>'.
                generate_login_formulaire()
                .'<script src="'.$script.'"></script>
            </body>
        ';
        return $body;
    }

    //----------Insertions de templates----------
?>
